irssi-script-pushover
=======================

Sends push notifications to your mobile devices using Pushover (https://pushover.net).
If AWAY, no notify will be sent.

Based on fnotify (https://gist.github.com/matthutchinson/542141).

How??
=====
Update the folders in the script (I use /home/kjelle/...)

Add fnotifyorig.pl to your .irssi/scripts/ folder, and symlink it to .irssi/scripts/autorun/

Add the crontab.txt to your cron to execute the pushover script.

Get an account at Pushover, and get your app on your mobile.

Update the User and Token keys. The User key you see on the Pushover page after you login. The Token is the key from an application. Click on "register an application", type a name, select "script" as plugin and choose an icon. The API Token/key will be shown in the app. 

Get someone to hilight or private message you to test. BLING!.. Turn on away (/away -all away). Retest (shouldn't generate a BLING on your mobile). Turn off away (/away).

You should be up and running :-i)

Next??
======
Call Pushover directly from the script (like http://www.weechat.org/scripts/source/pushover.pl.html/)

