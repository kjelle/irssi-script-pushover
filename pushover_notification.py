import httplib, urllib
import commands

__file__="/home/kjelle/.irssi/fnotifyorig"

def send_message(message):
    conn = httplib.HTTPSConnection("api.pushover.net:443")
    conn.request("POST", "/1/messages.json",
      urllib.urlencode({
        "token": "<you get this from pushover>",
        "user": "<you get this from pushover>",
        "message": str(message),
      }), { "Content-type": "application/x-www-form-urlencoded" })
    conn.getresponse()

try:
    try:
        lines = open(__file__).readlines()
    except Exception,e:
        import sys
        sys.exit(0)

    commands.getstatusoutput("rm -rf " + __file__ )
    for line in lines:
        send_message(line)

except Exception,e:
    raise
    pass



