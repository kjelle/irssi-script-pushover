# todo: grap topic changes

use strict;
use vars qw($VERSION %IRSSI);

use Irssi;
$VERSION = '0.1';
%IRSSI = (
    authors     => 'Kjell Tore  Fossbakk',
    contact     => 'fnotify@secwiz.org',
    name        => 'fnotifyorig',
    description => 'Write a notification to a file that shows who is talking to you in which channel.',
    url         => 'https://github.com/kjelle/irssi-script-pushover',
    license     => 'GNU General Public License',
    changed     => '$Date: 2014-04-22 09:45:00 +0100 (Tue, 22 Apr 2014) $'
);

#--------------------------------------------------------------------
# Parts based on fnotify.pl 0.0.3 by Thorsten Leehuis.
# http://www.leemhuis.info/files/fnotify/
#
# In parts based on knotify.pl 0.1.1 by Hugo Haas
# http://larve.net/people/hugo/2005/01/knotify.pl
# which is based on osd.pl 0.3.3 by Jeroen Coekaerts, Koenraad Heijlen
# http://www.irssi.org/scripts/scripts/osd.pl
#
# Other parts based on notify.pl from Luke Macken
# http://fedora.feedjack.org/user/918/
#
#--------------------------------------------------------------------

my $toggle_away ;
$toggle_away = 1 ;  

#--------------------------------------------------------------------
# Private message parsing
#--------------------------------------------------------------------

sub priv_msg {
    my ($server,$msg,$nick,$address,$target) = @_;
    filewrite($server, $nick." " .$msg );
}

#--------------------------------------------------------------------
# Printing hilight's
#--------------------------------------------------------------------

sub hilight {
    my ($dest, $text, $stripped) = @_;
    if ($dest->{level} & MSGLEVEL_HILIGHT) {
        filewrite($dest->{server}, $dest->{target}. " " .$stripped );
    }
}

#--------------------------------------------------------------------
# The actual printing
#--------------------------------------------------------------------

sub filewrite {
    my ($server, $text) = @_;

    if($toggle_away == 1 && !$server->{usermode_away}) {
        Irssi::print('fnotifyorig: blocked because you are not away.') ;
        return ;
    }


    open(FILE,">>$ENV{HOME}/.irssi/fnotifyorig");
    print FILE $text . "\n";
    close (FILE);
}


sub cmd_fnotify ($$$) {
    my ($args, $server, $witem) = @_ ;
    my @arg = split(/ /, $args) ;
    if (@arg == 0 || $arg[0] eq 'help') {
        my $help = $IRSSI{name}." ".$VERSION."
/fnotify toggle away        
    Switch if to obey away status on notification (default on).
";

        print $help ;
        print "---";

        if($toggle_away == 0) {
            print "Notifications will be written to file regardless of away status";
        } else {
            print "Notifications will only be wrtitten to file if you are away.";
        }

    } elsif ($args && $args eq 'toggle away' ) {
        $toggle_away = not $toggle_away ;
        if($toggle_away == 0) {
            print "Notifications will now be written to file regardless of away status";
        } else {
            print "Notifications will now only be wrtitten to file if you are away.";
        }
    } else {
        print $IRSSI{name}." ".$VERSION.": unknown command";
    }
}


#--------------------------------------------------------------------
# Irssi::signal_add_last / Irssi::command_bind
#--------------------------------------------------------------------

Irssi::signal_add_last("message private", "priv_msg");
Irssi::signal_add_last("print text", "hilight");

Irssi::command_bind('fnotify', \&cmd_fnotify) ;

foreach my $cmd ('toggle away') {
    Irssi::command_bind('fnotify '.$cmd => sub {
        cmd_fnotify("$cmd ".$_[0], $_[1], $_[2]); });
}

print CLIENTCRAP "%B>>%n ".$IRSSI{name}." ".$VERSION." loaded.";

#- end
